/**
 * @file
 * Gridster widget jQuery.
 */

var Drupal = Drupal || {};

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.gridsterWidget = {

    attach: function (context) {

      $(function (context) {
        var gridster = $(".gridster > div").addClass('gridster-processed').gridster({
          widget_selector: '.gridster-item',
          max_cols: 3,
          extra_rows: 3,
          widget_base_dimensions: [230, 230],
          widget_margins: [15, 15],
          resize: {
            enabled: true,
            stop: function (e, ui) {
              var data = gridster.serialize();
              ui.$helper.parent().parent().find('.gridster-item').each(function (i, o) {
                var gridsterData = data[i];
                $(this).find('input.field-top').val(gridsterData.row);
                $(this).find('input.field-left').val(gridsterData.col);
                $(this).find('input.field-height').val(gridsterData.size_y);
                $(this).find('input.field-width').val(gridsterData.size_x);
              });
            }
          },
          serialize_params: function ($w, wgd) {
            return {
              id: $($w).attr('class'),
              col: wgd.col,
              row: wgd.row,
              size_x: wgd.size_x,
              size_y: wgd.size_y
            };
          },
          draggable: {
            stop: function (e, ui) {
              getDataGridster(e, ui);
            }
          }
        }).data('gridster');

        if (gridster) {
          var gridsterItems = $(".gridster > div").gridster().data('gridster');
          var data = gridster.serialize();
          gridsterItems.$widgets.parent().find('.gridster-item').each(function (i, o) {
            var gridsterData = data[i];
            $(this).find('input.field-top').val(gridsterData.row);
            $(this).find('input.field-left').val(gridsterData.col);
            $(this).find('input.field-height').val(gridsterData.size_y);
            $(this).find('input.field-width').val(gridsterData.size_x);
          });
        }

        function getDataGridster(e, ui) {
          var data = gridster.serialize();
          ui.$helper.parent().find('.gridster-item').each(function (i, o) {
            var gridsterData = data[i];
            $(this).find('input.field-top').val(gridsterData.row);
            $(this).find('input.field-left').val(gridsterData.col);
            $(this).find('input.field-height').val(gridsterData.size_y);
            $(this).find('input.field-width').val(gridsterData.size_x);
          });
        }

      });
    }

  };

})(jQuery, Drupal);
