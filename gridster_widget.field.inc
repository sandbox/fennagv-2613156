<?php

/**
 * @file
 * Field module functionality for the File module.
 */

use Drupal\Core\Render\Element;
use Drupal\file\Entity\File;

/**
 * Returns HTML for an individual file upload widget.
 *
 * Default template: file-widget.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: A render element representing the file.
 */
function template_preprocess_gridster_widget(array &$variables) {

  global $base_url;

  $element = $variables['element'];

  foreach (Element::children($element) as $key) {
    $left = 3;
    $top = 5;
    $width = 1;
    $height = 1;

    $element[$key]['_weight'] = array();
    $element[$key]['preview'] = array();
    $element[$key]['#title'] = '';
    $element[$key]['alt']['#description'] = '';

    $field_name = $element[$key]['field_name']['#value'];
    $entity_type = $element[$key]['entity_type']['#value'];
    $entity_id = $element[$key]['entity_id']['#value'];

    if (isset($field_name) &&  $entity_type && $entity_id && isset($key) && isset($element[$key]['#default_value']['fids'][0])) {

      // Get data from db to set up attributes for gridster js.
      $result = db_select('gridster_widget')
        ->fields('gridster_widget')
        ->condition('delta', $key)
        ->condition('field_name', $field_name)
        ->condition('entity_type', $entity_type)
        ->condition('entity_id', $entity_id)
        ->execute()
        ->fetch();

      if ($result) {
        $left = $result->gridster_left;
        $top  = $result->gridster_top;
        $width = $result->gridster_width;
        $height = $result->gridster_height;
      }

      // Get uri image for background image!
      $fid = $element[$key]['#default_value']['fids'][0];

      if (isset($fid)) {
        $file_img = $fid;
        $file_url = File::load($file_img);
        $img_url = $base_url;
        $img_url .= '/sites/default/files/';
        $img_url .= $file_url->filename->value;
      }

      $element[$key] = array(
        '#type' => 'container',
        'item' => $element[$key],
        '#attributes' => array(
          'style' => array('background-image: url(' . $img_url . ');', 'background-size: cover; background-position: center center;'),
          'class' => array('gridster-item', 'item-' . $key . ''),
          'data-row' => array($top),
          'data-col' => array($left),
          'data-sizex' => array($width),
          'data-sizey' => array($height),
        ),
      );
    }
    else {
      $element[$key] = array(
        '#type' => 'container',
        'item' => $element[$key],
        '#attributes' => array(
          'class' => array('add'),
        ),
      );
    }
  }
  $variables['element'] = $element;

}
