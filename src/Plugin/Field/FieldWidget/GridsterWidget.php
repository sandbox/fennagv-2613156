<?php

/**
 * @file
 * Contains Drupal\gridster_widget\Plugin\Field\FieldWidget\GridsterFieldWidget.
 */

namespace Drupal\gridster_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;

/**
 * Plugin implementation of the 'gridster_image' widget.
 *
 * @FieldWidget(
 *  id = "gridster_image",
 *  label = @Translation("Gridster widget"),
 *  field_types = {
 *    "image"
 *  }
 * )
 */
class GridsterWidget extends ImageWidget {

  /**
   * {@inheritdoc}
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {

    // Implementing library for multiple elements.
    $elements = parent::formMultipleElements($items, $form, $form_state);

    $elements['#attached']['library'][] = 'gridster_widget/gridster';

    $elements['#theme'] = 'gridster_widget';

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    // Setting up the form, and needed input for gridster.
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_name = $element['#field_name'];
    $entity = $items->getParent()->getValue();
    $entity_type = $entity->getEntityTypeId();
    $entity_id   = $entity->id();

    $element['field_name'] = array(
      '#type' => 'hidden',
      '#value' => $field_name,
    );

    $element['entity_type'] = array(
      '#type' => 'hidden',
      '#value' => $entity_type,
    );

    $element['entity_id'] = array(
      '#type' => 'hidden',
      '#value' => $entity_id,
    );

    $element['left'] = array(
      '#type' => 'textfield',
      '#maxlength' => 6,
      '#size' => 6,
      '#attributes' => array(
        'class' => array('field-left', 'hidden'),
      ),
      '#required' => $element['#required'],
    );

    $element['top'] = array(
      '#type' => 'textfield',
      '#maxlength' => 6,
      '#size' => 6,
      '#attributes' => array(
        'class' => array('field-top', 'hidden'),
      ),
      '#required' => $element['#required'],
    );

    $element['gridster_height'] = array(
      '#type' => 'textfield',
      '#maxlength' => 6,
      '#size' => 6,
      '#attributes' => array(
        'class' => array('field-height', 'hidden'),
      ),
      '#required' => $element['#required'],
    );
    $element['gridster_width'] = array(
      '#type' => 'textfield',
      '#maxlength' => 6,
      '#size' => 6,
      '#attributes' => array(
        'class' => array('field-width', 'hidden'),
      ),
      '#required' => $element['#required'],
    );

    if (isset($delta) && $entity_type && $entity_id && $field_name) {

      $result = db_select('gridster_widget')
        ->fields('gridster_widget')
        ->condition('delta', $delta)
        ->condition('field_name', $element['field_name']['#value'])
        ->condition('entity_type', $element['entity_type']['#value'])
        ->condition('entity_id', $element['entity_id']['#value'])
        ->execute()
        ->fetch();

      $element['left']['#default_value'] = $result->gridster_left;
      $element['top']['#default_value'] = $result->gridster_top;
      $element['gridster_height']['#default_value'] = $result->gridster_height;
      $element['gridster_width']['#default_value'] = $result->gridster_width;

      if (!empty($result->gridster_top) == FALSE) {
        $element['left']['#default_value'] = '3';
        $element['top']['#default_value'] = '4';
        $element['gridster_height']['#default_value'] = '1';
        $element['gridster_width']['#default_value'] = '1';
      }

    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function process($element, FormStateInterface $form_state, $form) {

    // Set data from input into db.
    // TODO: look into presave/postsave.
    $element = parent::process($element, $form_state, $form);

    $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);

    $key = $element['#delta'];

    if (isset($value['field_name']) && $value['entity_type'] && $value['entity_id'] && isset($key)) {

      \Drupal::database()->merge('gridster_widget')
        ->key(array(
          'field_name' => $value['field_name'],
          'entity_type' => $value['entity_type'],
          'entity_id' => $value['entity_id'],
          'delta' => $key,
        ))
        ->fields(array(
          'gridster_top' => $value['top'],
          'gridster_left' => $value['left'],
          'gridster_width' => $value['gridster_width'],
          'gridster_height' => $value['gridster_height'],
        ))->execute();

      // TODO: there has to be a neater way to do this.
      \Drupal::database()->delete('gridster_widget')
        ->condition('entity_type', $value['entity_type'])
        ->condition('entity_id', $value['entity_id'])
        ->condition('field_name', $value['field_name'])
        ->condition('delta', $key, '>')
        ->execute();

    }

    return $element;
  }

}
