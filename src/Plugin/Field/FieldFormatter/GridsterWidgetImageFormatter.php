<?php

/**
 * @file
 * Contains Drupal\gridster_widget\Plugin\Field\FieldFormatter.
 */

namespace Drupal\gridster_widget\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'Gridster widget' formatter.
 *
 * @FieldFormatter(
 *  id = "gridster_widget_image_formatter",
 *  label = @Translation("Gridster image"),
 *  field_types = {"image"}
 * )
 */
class GridsterwidgetImageFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);

    $element['image_style'] = array(
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {

    $elements = array();

    $height_diff = 0;
    $height_diff_item = array();

    $image_style_setting = $this->getSetting('image_style');
    if (count($items) == 0) {
      return $elements;
    }

    // See if image style is required.
    $image_style = NULL;
    if (!empty($image_style_setting)) {
      $image_style = entity_load('image_style', $image_style_setting);
    }

    foreach ($items as $delta => $item) {

      if ($item->entity) {

        $image_uri = $item->entity->getFileUri();

        if ($image_style) {
          $image_uri = ImageStyle::load($image_style->getName())->buildUrl($image_uri);
        }
        else {
          $image_uri = $item->entity->url();
        }

        $grid_style = '';
        $get_id = Html::getUniqueId('gridster item');

        $entity = $items->getParent()->getValue();
        $entity_type = $entity->getEntityTypeId();
        $entity_id   = $entity->id();
        $field_name = $items->getName();

        // Get data from db for styling item.
        $results[$entity_id] = db_select('gridster_widget')
          ->fields('gridster_widget')
          ->condition('delta', $delta)
          ->condition('entity_id', $entity_id)
          ->condition('entity_type', $entity_type)
          ->condition('field_name', $field_name)
          ->execute()
          ->fetch();

        $result = $results[$entity_id];
        $height_diff_item[$height_diff++] = $result->gridster_top;
        $cols = 3;

        if ($result) {
          $left = ($result->gridster_left - 1) * (100 / $cols) + 1.5;
          $top = ($result->gridster_top - 1) * (100 / $cols);
          $width = ($result->gridster_width) * (100 / $cols) - 3;
          $height = ($result->gridster_height) * (100 / $cols) - 3;
          $grid_style .= 'left:' . $left . '%;';
          $grid_style .= 'top:' . $top . '%;';
          $grid_style .= 'width:' . $width . '%;';
          $grid_style .= 'height:' . $height . '%;';

          $element[$delta] = array(
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => array(
              'id' => $get_id,
              'class' => 'grister-image',
              'style' => 'background-image: url(' . $image_uri . ');',
            ),
          );
        }

        $element['#attached']['html_head'][] = [[
          '#tag' => 'style',
          '#value' => '#' . $get_id . ' { height:100%;' . $grid_style . '; }',
        ],
          'gridster_image_formatter_css_' . $get_id,
        ];
      }
    }

    // This need some fixing.
    // TODO get height from item.
    $max_height_item = max($height_diff_item);
    $margin_height = ($max_height_item - $cols) * (100 / $cols);
    $class = 'gridster-wrapper-' . $entity_id;

    $element['#attached']['html_head'][] = [[
      '#tag' => 'style',
      '#value' => '.' . $class . ' { margin-bottom: ' . $margin_height . '%; }',
    ],
      'gridster_image_css_' . $class,
    ];

    return $element;
  }

}
