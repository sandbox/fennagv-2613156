About Gridster widget
---------------------
This module adds a field widget, enabling <a href='http://gridster.net'>Gridster.js</a> functionality for image fields. With this module you can arrange images in a Gridster grid.

Installation
* Download and enable the module
* Download Gridster.js (https://github.com/ducksboard/gridster.js/zipball/master) and place gridster.css file in modules/gridster_widget/css and jquery.gridster.min.js in modules/gridster_widget/js
* Manage form display and enable the Gridster widget
* Manage display and enable the Gridster image formatter
* Edit the content and drag-and-drop the images into place

Features
--------
* Gridster widget for Image fields
* Gridster image formatter for Image fields

Credits
-------
Maintained and developed by Unc Inc (https://www.uncinc.nl)
